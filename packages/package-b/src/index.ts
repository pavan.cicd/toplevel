import {getContentsAsJson} from '@rk/utils';
import path from 'path';

export async function printPackageNameWithVersion(filePath?: string) {
  const packageJson = await getContentsAsJson(
    filePath ?? path.join(__dirname, '..', 'package.json'),
  );
  const result = packageJson.name + '@' + packageJson.version;
  return result;
}
