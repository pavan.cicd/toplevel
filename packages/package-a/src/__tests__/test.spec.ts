import {expect} from 'chai';
import {printPackageNameWithVersion} from '..';
import path from 'path';

describe('package-a tests', () => {
  it('valid case', async () => {
    const res = await printPackageNameWithVersion();
    expect(res).to.equal('@rk/package-a@1.0.0');
  });

  it('should throw and error if invalid config is passed', async () => {
    try {
      await printPackageNameWithVersion(path.join(__dirname, 'test.spec.js'));
      expect(1).to.equal(0);
    } catch (error) {
      expect(error.name).to.equal('SyntaxError');
    }
  });
});
