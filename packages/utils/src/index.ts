import fs from 'fs/promises';
export async function getContentsAsJson(filePath: string) {
  const content = await fs.readFile(filePath, {encoding: 'utf-8'});
  const result = JSON.parse(content);
  return result;
}
