import {expect} from 'chai';
import {getContentsAsJson} from '..';
import path from 'path';

describe('package-a tests', () => {
  it('valid case', async () => {
    const res = await getContentsAsJson(
      path.join(__dirname, '..', '..', 'package.json'),
    );
    expect(res.version).to.equal('1.0.0');
  });
});
