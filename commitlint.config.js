const isCI = process.env.CI;

module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'header-max-length': [2, 'always', 100],
    'body-leading-blank': [2, 'always'],
    'footer-leading-blank': [0, 'always'],
    'signed-off-by': [isCI != null ? 0 : 2, 'always', 'Signed-off-by:'],
  },
};
